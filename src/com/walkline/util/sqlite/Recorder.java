package com.walkline.util.sqlite;

import java.util.Date;

public final class Recorder
{
	private int _id;
	private Date _paymentDate;
	private int _currency;
	private float _amount;
	private int _category;
	private String _detail;
	private int _usedCard;

	public Recorder(int id, Date paymentDate, int currency, float amount, int category, String detail, int usedCard)
	{
		_id = id;
		_paymentDate = paymentDate;
		_currency = currency;
		_amount = amount;
		_category = category;
		_detail = detail;
		_usedCard =  usedCard;
	}

	/**
	 * 获取当前记录编号
	 * @return 记录编号
	 */
	int getID() {return _id;}

	/**
	 * 获取当前记录消费日期
	 * @return 消费日期
	 */
	Date getPaymentDate() {return _paymentDate;}

	/**
	 * 获取当前记录消费币种
	 * @return 消费币种
	 */
	int getCurrency() {return _currency;}

	/**
	 * 获取当前记录消费金额
	 * @return 消费金额
	 */
	float getAmount() {return _amount;}

	/**
	 * 获取当前记录消费分类
	 * @return 消费分类
	 */
	int getCategory() {return _category;}

	/**
	 * 获取当前记录消费明细
	 * @return 消费明细
	 */
	String getDetail() {return _detail;}

	/**
	 * 获取当前记录使用的卡片
	 * @return 使用的卡片
	 */
	int getUsedCard() {return _usedCard;}
}
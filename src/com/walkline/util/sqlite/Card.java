package com.walkline.util.sqlite;

public final class Card
{
	private int _id;
	private String _bankName;
	private String _tailNumber;
	private long _accounts;
	private long _repayment;
	private String _phoneNumber;

	public Card(int id, String bankName, String tailNumber, long accounts, long repayment, String phoneNumber)
	{
		_id = id;
		_bankName = bankName;
		_tailNumber = tailNumber;
		_accounts = accounts;
		_repayment = repayment;
		_phoneNumber = phoneNumber;
	}

	/**
	 * 获取当前卡片编号
	 * @return 卡片编号
	 */
	public int getID() {return _id;}

	/**
	 * 获取当前卡片发卡行
	 * @return 卡片发卡行
	 */
	public String getBankName() {return _bankName;}

	/**
	 * 获取当前卡片尾号
	 * @return 卡片尾号
	 */
	public String getTailNumber() {return _tailNumber;}

	/**
	 * 获取当前卡片出账日
	 * @return 卡片出账日
	 */
	public long getAccountsDay() {return _accounts;}

	/**
	 * 获取当前卡片还款日
	 * @return 卡片还款日
	 */
	public long getRepaymentDay() {return _repayment;}

	/**
	 * 获取当前卡片客服电话
	 * @return 客户电话
	 */
	public String getPhoneNumber() {return _phoneNumber;}
}
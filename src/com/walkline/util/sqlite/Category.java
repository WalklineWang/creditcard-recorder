package com.walkline.util.sqlite;

public final class Category
{
	private int _id;
	private String _showAsChinese;
	private String _showAsEnglish;

	public Category(int id, String showAsChinese, String showAsEnglish)
	{
		_id = id;
		_showAsChinese = showAsChinese;
		_showAsEnglish = showAsEnglish;
	}

	/**
	 * 获取当前分类编号
	 * @return 分类编号
	 */
	public int getID() {return _id;}

	/**
	 * 获取当前分类中文名称
	 * @return 分类中文名称
	 */
	public String getChinese() {return _showAsChinese;}

	/**
	 * 获取当前分类英文名称
	 * @return 分类英文名称
	 */
	public String getEnglish() {return _showAsEnglish;}
}
package com.walkline.util.sqlite;

public final class Currency
{
	private int _id;					//编号
	private String _showAsChinese;		//货币中文名称
	private String _showAsAbbreviation;	//货币英文缩写
	private String _showAsSymbol;		//货币符号
	private int _defaultItem;			//默认显示项目

	public Currency(int id, String showAsChinese, String showAsAbbreviation, String showAsSymbol, int defaultItem) 
	{
		_id = id;
		_showAsChinese = showAsChinese;
		_showAsAbbreviation = showAsAbbreviation;
		_showAsSymbol = showAsSymbol;
		_defaultItem = defaultItem;
	}

	/**
	 * 获取当前货币编号
	 * @return 货币编号
	 */
	public int getID() {return _id;}

	/**
	 * 获取当前货币中文名称
	 * @return 货币中文名称
	 */
	public String getChinese() {return _showAsChinese;}

	/**
	 * 获取当前货币英文缩写
	 * @return 货币英文缩写
	 */
	public String getAbbreviation() {return _showAsAbbreviation;}

	/**
	 * 获取当前货币符号
	 * @return 货币符号
	 */
	public String getSymbol() {return _showAsSymbol;}

	/**
	 * 判断当前货币是否为默认货币
	 * @return true为默认货币，false为非默认货币
	 */
	public boolean isDefault() {return (_defaultItem <= 0 ? false : true);}
}
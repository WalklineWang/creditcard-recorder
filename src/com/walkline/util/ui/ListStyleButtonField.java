package com.walkline.util.ui;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Characters;
import net.rim.device.api.system.Display;
import net.rim.device.api.system.KeypadListener;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.TouchEvent;

import com.walkline.app.CreditcardRecorderConfig;

public class ListStyleButtonField extends Field
{
    public static final int DRAWPOSITION_TOP = 0;
    public static final int DRAWPOSITION_BOTTOM = 1;
    public static final int DRAWPOSITION_MIDDLE = 2;
    public static final int DRAWPOSITION_SINGLE = 3;
    
    private static final int LISTSTYLE_ALARMCLOCK=0;
    private static final int LISTSTYLE_WORLDCLOCK=1;
    private static final int LISTSTYLE_REMINDER=2;
    
    private static final int CORNER_RADIUS = 12;
    
    private static final int HPADDING = Display.getWidth() <= 320 ? 4 : 6;
    private static final int VPADDING = 4;
    
    private static final boolean USING_LARGE_ICON = Display.getWidth()<640 ? false : true;
    
    private static int COLOR_BACKGROUND = 0xFFFFFF;
    private static int COLOR_BORDER = 0xBBBBBB;
    private static int COLOR_BACKGROUND_FOCUS = 0x186DEF;
    //private static final int COLOR_LIST_DISABLED = Color.DARKGRAY;//0x6D6D6D;//0x6A6A6A;

    //private Bitmap _leftIcon;
    private Bitmap _iconNormal = USING_LARGE_ICON ? Bitmap.getBitmapResource("titleIcon_large.png") : Bitmap.getBitmapResource("titleIcon_small.png");

    private MyLabelField _labelTitle;
    //private MyLabelField _labelDate;

    private int _rightOffset;
    private int _leftOffset;
    private int _labelHeight;
    //private int _listStyle;
    
    private int _drawPosition = -1;
    
	//private static PersistentObject _store;
	//private static Vector _data;
	//private static int _fontSize=1;

	private static Font FONT_TITLE = CreditcardRecorderConfig.FONT_LIST_TITLE;

    public ListStyleButtonField(String title, Bitmap bitmap)
    {
        super(USE_ALL_WIDTH | Field.FOCUSABLE);

        _labelTitle = new MyLabelField(title);
        _labelTitle.setFont(FONT_TITLE);
        /*
        _iconNormal = getFileExtIcon(fileExt.toLowerCase());
        */
    }

    public void setDrawPosition(int drawPosition) {_drawPosition = drawPosition;}

    public void layout(int width, int height)
    {
        _leftOffset = _iconNormal.getWidth() + HPADDING*2;
        _rightOffset = HPADDING;
        
        _labelTitle.layout(width - _leftOffset - _rightOffset, height);
        _labelHeight = _labelTitle.getHeight();

        setExtent(width, _labelHeight + 20);//+2* extraVPaddingNeeded);
    }

    protected void paint(Graphics g)
    {
        // Logo Bitmap
   		g.drawBitmap(HPADDING, (getHeight()-_iconNormal.getHeight())/2, _iconNormal.getWidth(), _iconNormal.getHeight(), _iconNormal, 0, 0);
        
        // Caption Text
        try
        {
        	g.setFont(FONT_TITLE);
        	g.pushRegion(_iconNormal.getHeight() + HPADDING * 2, (getHeight() - _labelTitle.getHeight()) / 2, _labelTitle.getWidth(), _labelTitle.getHeight(), 0, 0);
        	_labelTitle.paint(g);
        } finally {g.popContext();}
    }

    protected void paintBackground(Graphics g)
    {
        if(_drawPosition < 0)
        {
            super.paintBackground(g);
            return;
        }

        int oldColour = g.getColor();

        switch(0)
        {
			case LISTSTYLE_ALARMCLOCK:
        		COLOR_BACKGROUND_FOCUS = 0x186DEF;

        		break;
			case LISTSTYLE_WORLDCLOCK:
        		COLOR_BACKGROUND_FOCUS = 0x0A9000;

        		break;
			case LISTSTYLE_REMINDER:
        		COLOR_BACKGROUND_FOCUS = 0xC72F00;

        		break;
		}

        int background = g.isDrawingStyleSet(Graphics.DRAWSTYLE_FOCUS) ? COLOR_BACKGROUND_FOCUS : COLOR_BACKGROUND;

        try {
            if(_drawPosition == 0)
            {
                // Top
                g.setColor(background);
                g.fillRoundRect(0, 0, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, 0, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
            } else if(_drawPosition == 1)
            {
                // Bottom 
                g.setColor(background);
                g.fillRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
            } else if(_drawPosition == 2)
            {
                // Middle
                g.setColor(background);
                g.fillRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + 2 * CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, -CORNER_RADIUS, getWidth(), getHeight() + 2 * CORNER_RADIUS, CORNER_RADIUS, CORNER_RADIUS);
                g.drawLine(0, getHeight() - 1, getWidth(), getHeight() - 1);
            } else {
                // Single
                g.setColor(background);
                g.fillRoundRect(0, 0, getWidth(), getHeight(), CORNER_RADIUS, CORNER_RADIUS);
                g.setColor(COLOR_BORDER);
                g.drawRoundRect(0, 0, getWidth(), getHeight(), CORNER_RADIUS, CORNER_RADIUS);
            }
        } finally {
            g.setColor(oldColour);
        }
    }
    
    protected void drawFocus(Graphics g, boolean on)
    {
        if(_drawPosition < 0)
        {
        } else {
            boolean oldDrawStyleFocus = g.isDrawingStyleSet(Graphics.DRAWSTYLE_FOCUS);
            try {
                if(on)
                {
                	g.setColor(Color.WHITE);
                    g.setDrawingStyle(Graphics.DRAWSTYLE_FOCUS, true);
                }
                paintBackground(g);
                paint(g);
            } finally {
                g.setDrawingStyle(Graphics.DRAWSTYLE_FOCUS, oldDrawStyleFocus);
            }
        }
    }

    protected boolean keyChar(char character, int status, int time) 
    {
    	switch (character)
    	{
			case Characters.ENTER:
	            clickButton();
	            return true;
        }

        return super.keyChar(character, status, time);
    }

    protected boolean navigationUnclick(int status, int time) 
    {
    	if ((status & KeypadListener.STATUS_FOUR_WAY) == KeypadListener.STATUS_FOUR_WAY)
    	{
        	clickButton();
        	return true;
    	}

    	return super.navigationClick(status, time);
    }

    protected boolean trackwheelClick(int status, int time)
    {
    	if ((status & KeypadListener.STATUS_TRACKWHEEL) == KeypadListener.STATUS_TRACKWHEEL)
    	{
        	clickButton();
        	return true;
    	}

    	return super.trackwheelClick(status, time);
    }

    protected boolean invokeAction(int action) 
    {
    	switch(action)
    	{
    		case ACTION_INVOKE:
            	clickButton();
            	return true;
    	}

    	return super.invokeAction(action);
    }

    protected boolean touchEvent(TouchEvent message)
    {
        int x = message.getX(1);
        int y = message.getY(1);

        if (x < 0 || y < 0 || x > getExtent().width || y > getExtent().height) {return false;}

        switch (message.getEvent())
        {
            case TouchEvent.UNCLICK:
            	clickButton();
            	return true;
        }

        return super.touchEvent(message);
    }

    public void clickButton() {fieldChangeNotify(0);}

    public void setDirty(boolean dirty) {}
    public void setMuddy(boolean muddy) {}
}
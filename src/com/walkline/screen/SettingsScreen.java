package com.walkline.screen;

import com.walkline.util.sqlite.SQLManager;
import com.walkline.util.ui.ForegroundManager;
import com.walkline.util.ui.ListStyleButtonField;
import com.walkline.util.ui.ListStyleButtonSet;

import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.container.MainScreen;

public class SettingsScreen extends MainScreen
{
	private SQLManager _sqlManager;
	private ForegroundManager _foreground = new ForegroundManager(0);

	public SettingsScreen(SQLManager sqlManager)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT | NO_SYSTEM_MENU_ITEMS);

		setTitle("Settings");

		_sqlManager = sqlManager;

		ListStyleButtonSet _listSet = new ListStyleButtonSet();
		ListStyleButtonField _itemCurrency = new ListStyleButtonField("Currency", null);
		ListStyleButtonField _itemCategory = new ListStyleButtonField("Category", null);
		ListStyleButtonField _itemCard = new ListStyleButtonField("Card", null);
		ListStyleButtonField _itemShotrcutKey = new ListStyleButtonField("Shortcut Key", null);
		ListStyleButtonField _itemDefaultAction = new ListStyleButtonField("Default Action", null);

		_listSet.add(_itemCurrency);
		_listSet.add(_itemCategory);
		_listSet.add(_itemCard);
		_listSet.add(_itemShotrcutKey);
		_listSet.add(_itemDefaultAction);

		_itemCurrency.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				UiApplication.getUiApplication().pushScreen(new SettingsCurrencyScreen(_sqlManager));
			}
		});

		_itemCategory.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				UiApplication.getUiApplication().pushScreen(new SettingsCategoryScreen(_sqlManager));
			}
		});

		_itemCard.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				UiApplication.getUiApplication().pushScreen(new SettingsCardScreen(_sqlManager));
			}
		});

		_foreground.add(_listSet);
		add(_foreground);
	}
}
package com.walkline.screen;

import net.rim.device.api.i18n.SimpleDateFormat;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.DateField;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.MainScreen;

import com.walkline.util.sqlite.SQLManager;
import com.walkline.util.ui.ForegroundManager;
import com.walkline.util.ui.RoundRectFieldManager;

public class NewRecorderScreen extends MainScreen
{
	private SQLManager _sqlManager;
	private ForegroundManager _foreground = new ForegroundManager(0);
	private RoundRectFieldManager _roundrect = new RoundRectFieldManager();

	public NewRecorderScreen(SQLManager sqlManager)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT | NO_SYSTEM_MENU_ITEMS);

		setTitle("Add new recorder");

		_sqlManager = sqlManager;

		//消费日期
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		DateField _dateField = new DateField("Date: ", System.currentTimeMillis(), sdf);

		//货币种类
		String[] choiceCurrency = {"CNY", "USD"};
		ObjectChoiceField _currencyField = new ObjectChoiceField("Currency: ", choiceCurrency, 0);

		//金额
		BasicEditField _amountField = new BasicEditField("Amounts: ", null, 8, BasicEditField.FIELD_RIGHT | BasicEditField.NO_NEWLINE | BasicEditField.FILTER_REAL_NUMERIC | BasicEditField.USE_ALL_WIDTH);

		//消费类型
		String[] choiceCategory = {"Toy", "Dinner", "Books", "Movies"};
		ObjectChoiceField _categoryField = new ObjectChoiceField("Category: ", choiceCategory, 0);

		//消费明细
		EditField _detailsfField = new EditField("Details: ", null, EditField.DEFAULT_MAXCHARS, EditField.USE_ALL_WIDTH);

		//刷卡卡片
		String[] choiceCards = {"9113", "8045"};
		ObjectChoiceField _cardsField = new ObjectChoiceField("Card: ", choiceCards, 0);

		_roundrect.add(_dateField);
		_roundrect.add(_amountField);
		_roundrect.add(_currencyField);
		_roundrect.add(_categoryField);
		_roundrect.add(_detailsfField);
		_roundrect.add(_cardsField);
		_foreground.add(_roundrect);
		add(_foreground);

		UiApplication.getUiApplication().invokeLater(new Runnable()
		{
			public void run()
			{
				initializeNewForm();
			}
		});
	}

	/**
	 * 初始化默认设置
	 */
	private void initializeNewForm()
	{
		
	}
}
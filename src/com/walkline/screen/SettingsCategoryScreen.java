package com.walkline.screen;

import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.EditField;
import net.rim.device.api.ui.component.ObjectChoiceField;
import net.rim.device.api.ui.container.MainScreen;

import com.walkline.util.sqlite.Category;
import com.walkline.util.sqlite.SQLManager;
import com.walkline.util.ui.ForegroundManager;

public class SettingsCategoryScreen extends MainScreen
{
	private SQLManager _sqlManager;
	private ForegroundManager _foreground = new ForegroundManager(0);

	private ObjectChoiceField _choiceCategoryID;
	private EditField _showAsChinese;
	private EditField _showAsEnglish;

	public SettingsCategoryScreen(SQLManager sqlManager)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT | NO_SYSTEM_MENU_ITEMS);

		setTitle("Category Settings");

		_sqlManager = sqlManager;

		_choiceCategoryID = new ObjectChoiceField("IDs: ", null, 0) {
			protected void fieldChangeNotify(int context)
			{
				if (context != FieldChangeListener.PROGRAMMATIC)
				{
					queryFirstRecorderDetails();
				}
			};
		};
		_showAsChinese = new EditField("Show as Chinese: ", null, EditField.DEFAULT_MAXCHARS, EditField.USE_ALL_WIDTH);
		_showAsEnglish = new EditField("Show as English: ", null, EditField.DEFAULT_MAXCHARS, EditField.USE_ALL_WIDTH);

		_foreground.add(_choiceCategoryID);
		_foreground.add(_showAsChinese);
		_foreground.add(_showAsEnglish);
        add(_foreground);

        UiApplication.getUiApplication().invokeLater(new Runnable()
        {
			public void run()
			{
				String[] _currencies = _sqlManager.getCategoriesID();
				_choiceCategoryID.setChoices(_currencies);

				queryFirstRecorderDetails();
			}
		});
	}

	private void queryFirstRecorderDetails()
	{
		String currentID = _choiceCategoryID.getChoice(_choiceCategoryID.getSelectedIndex()).toString();

		if (!currentID.equalsIgnoreCase("none"))
		{
			Category currency = _sqlManager.getCategoryByID(Integer.parseInt(currentID));

			if (currency != null)
			{
				_showAsChinese.setText(currency.getChinese());
				_showAsEnglish.setText(currency.getEnglish());
			}
		}
	}
}
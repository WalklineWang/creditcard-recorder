package com.walkline.screen;

import net.rim.device.api.system.Bitmap;
import net.rim.device.api.system.Display;
import net.rim.device.api.ui.Field;
import net.rim.device.api.ui.FieldChangeListener;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.FontFamily;
import net.rim.device.api.ui.FontManager;
import net.rim.device.api.ui.Ui;
import net.rim.device.api.ui.UiApplication;
import net.rim.device.api.ui.component.BitmapField;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

import com.walkline.app.CreditcardRecorderConfig;
import com.walkline.util.sqlite.SQLManager;
import com.walkline.util.ui.ForegroundManager;
import com.walkline.util.ui.MenuListStyleButtonField;
import com.walkline.util.ui.MenuListStyleButtonSet;

public final class CreditcardRecorderScreen extends MainScreen
{
	private static boolean _useLargeIcon = Display.getWidth() >= 640;
	//private static Bitmap _bitmapMoreInfo = _useLargeIcon ? Bitmap.getBitmapResource("moreInfo_large.png") : Bitmap.getBitmapResource("moreInfo_small.png");
	private static Bitmap _bitmapAppTitle = _useLargeIcon ? Bitmap.getBitmapResource("titleIcon_large.png") : Bitmap.getBitmapResource("titleIcon_small.png");

	private SQLManager _sqlManager;
	private ForegroundManager _foreground = new ForegroundManager(0);

    public CreditcardRecorderScreen(SQLManager sqlManager)
    {
    	super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT | NO_SYSTEM_MENU_ITEMS);

    	try {
			FontFamily family = FontFamily.forName("BBGlobal Sans");
			Font appFont = family.getFont(Font.PLAIN, 8, Ui.UNITS_pt);
			FontManager.getInstance().setApplicationFont(appFont);
		} catch (ClassNotFoundException e) {}

		BitmapField bmpTitleField = new BitmapField(_bitmapAppTitle);
		bmpTitleField.setSpace(5, 5);
		LabelField labelTitleField = new LabelField(CreditcardRecorderConfig.APP_TITLE, LabelField.ELLIPSIS);
		labelTitleField.setFont(CreditcardRecorderConfig.FONT_MAIN_TITLE);

		HorizontalFieldManager hfm = new HorizontalFieldManager(USE_ALL_WIDTH);
		VerticalFieldManager vfm = new VerticalFieldManager(FIELD_VCENTER);
		vfm.add(labelTitleField);
		hfm.add(bmpTitleField);
		hfm.add(vfm);
		hfm.setBackground(CreditcardRecorderConfig.bgColor_Gradient);
		setTitle(hfm);

        _sqlManager = sqlManager;

        MenuListStyleButtonField _mainMenuAddRecorder = new MenuListStyleButtonField("Add Recorder", null);
        MenuListStyleButtonField _mainMenuQueryRecorders = new MenuListStyleButtonField("Query Recorders", null);
        MenuListStyleButtonField _mainMenuSettings = new MenuListStyleButtonField("Settings", null);
        MenuListStyleButtonSet _mainMenuSet1 = new MenuListStyleButtonSet();
        MenuListStyleButtonSet _mainMenuSet2 = new MenuListStyleButtonSet();
        MenuListStyleButtonSet _mainMenuSet3 = new MenuListStyleButtonSet();

        _mainMenuSet1.add(_mainMenuAddRecorder);
        _mainMenuSet2.add(_mainMenuQueryRecorders);
        _mainMenuSet3.add(_mainMenuSettings);

        _foreground.add(_mainMenuSet1);
        _foreground.add(_mainMenuSet2);
        _foreground.add(new LabelField(null, NON_FOCUSABLE));
        _foreground.add(_mainMenuSet3);

        _mainMenuAddRecorder.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				UiApplication.getUiApplication().pushScreen(new NewRecorderScreen(_sqlManager));
			}
		});

        _mainMenuQueryRecorders.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				UiApplication.getUiApplication().pushScreen(new QueryRecorderScreen(_sqlManager));
			}
		});

        _mainMenuSettings.setChangeListener(new FieldChangeListener() {
			public void fieldChanged(Field field, int context)
			{
				UiApplication.getUiApplication().pushScreen(new SettingsScreen(_sqlManager));
			}
		});

        add(_foreground);
    }
}
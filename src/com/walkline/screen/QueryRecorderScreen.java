package com.walkline.screen;

import com.walkline.util.sqlite.SQLManager;

import net.rim.device.api.ui.container.MainScreen;

public class QueryRecorderScreen extends MainScreen
{
	private SQLManager _sqlManager;

	public QueryRecorderScreen(SQLManager sqlManager)
	{
		super(NO_VERTICAL_SCROLL | USE_ALL_HEIGHT | NO_SYSTEM_MENU_ITEMS);

		setTitle("Query recorders");

		_sqlManager = sqlManager;
	}
}